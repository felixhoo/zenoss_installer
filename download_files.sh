#!/bin/bash
download_file() {
  local file=$1
  local url=$2

  echo "==> Downloading $file from $url"
  curl -Ls $url -o $file
}

while IFS='|' read file md5 url
do
  if [ ! -f $file ]; then
    download_file $file $FS_BASE_URL/$url
  else
    localfile_md5=`md5sum $file |cut -d ' ' -f 1`
    if [ "$md5" = "$localfile_md5" ]; then
      echo "skip $file"
    else
      download_file $file $url
    fi
  fi
done < "files.index"
